/**
 * edited by Ari Talala on 14.9.2020
 * 
 * */



using System;
using System.Globalization;

namespace FuusorNayte
{
    public class WeatherForecast
    {

     //   public DateTime Date { get; set; }          // this is removed because this is too complex to handle as a date as a string is easier to save to and load from json.

        public int Id { get; set; }
        public int Wind { get; set; }

        public int Rain { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string City { get; set; }

        public string Date { get; set; }


        public WeatherForecast(string date, int wind, int rain, int temperatureC, string city)
        {
            
            Date = date;
            Wind = wind;
            Rain = rain;
            TemperatureC = temperatureC;
            City = city;


        }

        public WeatherForecast() { }

    }


    
    }

