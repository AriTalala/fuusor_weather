﻿/**
 * author Ari Talala 2020
 * Id is set here but it is not used as yet distinction between locations isnt necessary.
 * 
 * */

using System;
namespace FuusorNayte
{
    public class WeatherLocation
    {
        public WeatherLocation(string name)
        {
            Name = name;
        }

        public WeatherLocation(string name, int id) {

            Name = name;
            Id = id;
        }


        public WeatherLocation() { }

        public int Id { get; set; }

        public string Name { get; set; }


  


    }
}
