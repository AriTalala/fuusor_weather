import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent {
  public forecasts: WeatherForecast[];
  public urlString: string;
  public selectedForecast: string;
  private http: HttpClient;
  public baseUrl = "https://localhost:5001/"; //I use this because using app.module.ts function getBaseUrl() produces circular reference error


  public currentCount = 0;

  // this constructor builds a get request which corresponds to WeatherForecastControllers [HttpGET]
  // designated function call. How this is exactly accomplished, linking this request to Controller,
  // escapes my knowledge as of yet....
  
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

    this.http = http;

    
    this.http.get<WeatherForecast[]>(baseUrl + 'weatherforecast').subscribe(result => {
      this.forecasts = result;
      this.urlString = baseUrl;
      this.baseUrl = baseUrl;
    }, error => console.error(error));
  }

  public doThis() {

  }

  public ForecastSelected(u: any) {
    this.selectedForecast = u;   // declare variable in component.
  }

  /**
   * This is used to send a httpDelete request
   * @param id
   */
  public Remove(forecast) {

    this.currentCount = forecast.id;

    

    this.http.delete<WeatherForecast[]>(this.baseUrl + 'weatherforecast' + '/' + forecast.id).subscribe(result => {
      console.log("RESULT", result);
      this.forecasts = result;
    }, error => console.error(error));

    
  }
  
}

interface WeatherForecast {
  id: number;
  date: string;
  temperatureC: number;
  temperatureF: number;
  wind: number;
  rain: number;
  city: string;
}
