import { Component, Inject } from '@angular/core';
import { HttpClient, HttpHandler, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';




@Component({
  selector: 'add-forecast.component',
  templateUrl: './add-forecast.component.html'
})
export class ManageForecasts {
  public currentCount = 0;
  public url = "";
  public okay = "";
  public count = 0;

  public baseUrl = "https://localhost:5001/"; //I use this because using app.module.ts function getBaseUrl() produces circular reference error
  private http: HttpClient;

  public locations: WeatherLocation[];
  public forecasts: WeatherForecast[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

    this.http = http;

    this.http.get<WeatherLocation[]>(baseUrl + 'weatherlocation').subscribe(result => {
      this.locations = result;
      this.url = baseUrl;
      this.okay = this.url;
    }, error => console.error(error));
  }


  public incrementCounter() {
    this.currentCount++;
  }

  public setDate(date: string) {
    this.okay = date;

  }

  public removeForecast(name: string) {

  }

  public addForecastModified() {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'

      })
    };


    const body = JSON.stringify({ "name": "Lahti" });



    this.http.post<WeatherLocation[]>(this.baseUrl + 'weatherlocation',
      body, httpOptions
    ).subscribe(result => {
      console.log("RESULT", result);
      this.locations = result;
    }, error => console.error(error));


  }

  public addForecast(date: string, cityname: string, wind: number, rain: number, temperaturec: number) {


    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'

      })
    };

      const body = JSON.stringify(
        {
          "Id": 0,
          "Date": date,
          "City": cityname,
          "TemperatureC": new Number(temperaturec),
          "Rain": new Number(rain),
          "Wind": new Number(wind),
        }
      );

    
    this.http.post<WeatherForecast[]>(this.baseUrl + 'weatherforecast',
      body, httpOptions
    ).subscribe(result => {
      console.log("RESULT", result);
      this.forecasts = result;
    }, error => console.error(error));

    
  }

  public editForecast(name: string) {

  }


  
}

interface WeatherLocation {

  name: string;
}

interface WeatherForecast {
  id: number;
  date: string;
  temperatureC: number;
  temperatureF: number;
  wind: number;
  rain: number;
  city: string;
}
