import { Component, Inject } from '@angular/core';
import { HttpClient, HttpHandler, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';



@Component({
  selector: 'add-city-component',
  templateUrl: './add-city.component.html'
})
export class ManageCities {
  public currentCount = 0;
  public city = "000";
  public baseUrl = "https://localhost:5001/"; //I use this because using app.module.ts function getBaseUrl() produces circular reference error
  private http: HttpClient;

  public locations: WeatherLocation[];
  

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.http = http;
    this.baseUrl = baseUrl;

    this.http.get<WeatherLocation[]>(baseUrl + 'weatherlocation').subscribe(result => {
      this.locations = result;
    }, error => console.error(error));

  }



  public removeCity(id: number) {

    
    this.http.delete<WeatherLocation[]>(this.baseUrl + 'weatherlocation' + '/' + id).subscribe(result => {
      console.log("RESULT", result);
      this.locations = result;
    }, error => console.error(error));
    
    
  }


  public addCity(cityname: string) {

   // let headers: HttpHeaders = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    
    
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
        
      })
    };

    
    const body = JSON.stringify({ "name": cityname });


    
    this.http.post<WeatherLocation[]>(this.baseUrl + 'weatherlocation',
     body , httpOptions
    ).subscribe(result => {
      console.log("RESULT", result);
      this.locations = result;
    }, error => console.error(error));
    
    

  }


}

interface WeatherLocation {
  id: number;
  name: string;
  
}



