﻿/**
 * author Ari Talala 2020
 * 
 * This class is used in WeatherForecastController to dispatch saves and fetching of WeatherForecasts.
 * This class is sort of an interface and could be used to store data to DB instead of local files.
 * 
 * */

using System;

using System.Collections.Generic;

namespace FuusorNayte
{
    public class WeatherForecastManager
    {

        public List<WeatherForecast> ForecastsContainer = new List<WeatherForecast>();
        public WeatherForecastManager()
        {
        }


        public void addForecast(WeatherForecast forecast) {

            //ForecastsContainer.Add(forecast);
            JsonStorageManager jsonStorageManager = new JsonStorageManager();
            jsonStorageManager.saveNewForecast(forecast);
        }

        public void removeForecast(int index) {
            // ForecastsContainer.RemoveAt(index);
            JsonStorageManager manager = new JsonStorageManager();
            //manager.

        }

        public List<WeatherForecast> getWeatherForecasts() {
            return ForecastsContainer;
        }
    }
}
