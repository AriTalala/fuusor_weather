﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
/*
 This class is used to store cities and forecasts to JSON files and fetch forecast / city data from files.
 
 */
namespace FuusorNayte
{
    public class JsonStorageManager
    {
        private string CitiesJson = "./cities.json";
        private string ForecastsJson = "./forecasts.json";


        public JsonStorageManager()
        {
            
            
        }

        
        public List<WeatherLocation> saveNewLocation(WeatherLocation location) {

            List<WeatherLocation> filecontent = getLocationsAsList();
            filecontent.Add(location);
          
            string newJson = JsonConvert.SerializeObject(filecontent, Formatting.Indented);
           
            System.IO.File.WriteAllText(CitiesJson, newJson);

            return getLocationsAsList();
        }

        /*
         * Saves a new forecast to a file and returns the updated forecasts list from a file.
         
         */
        public List<WeatherForecast> saveNewForecast(WeatherForecast forecast)
        {

            List<WeatherForecast> filecontent = getForecastsAsList();
            filecontent.Add(forecast);
            

            string newJson = JsonConvert.SerializeObject(filecontent, Formatting.Indented);
            

            System.IO.File.WriteAllText(ForecastsJson, newJson);


            return getForecastsAsList();
        }

        /*
         Here we remove a forecast from JSON-file matching the id.
        First we de-serialize JSON file to a list.
        Remove the item from a list.
        then serialize list back to a JSON-compliant string which is in turn written to file.
         */

        public List<WeatherForecast> removeForecast(int id)
        {

            List<WeatherForecast> UpdatedList = removeForecastFromList(id);

            string newJson = JsonConvert.SerializeObject(UpdatedList, Formatting.Indented);

            System.IO.File.WriteAllText(ForecastsJson, newJson);

            return getForecastsAsList();
        }

        public List<WeatherForecast> removeCity(int id)
        {

            List<WeatherLocation> UpdatedList = removeCityFromList(id);

            string newJson = JsonConvert.SerializeObject(UpdatedList, Formatting.Indented);

            System.IO.File.WriteAllText(CitiesJson, newJson);

            return getForecastsAsList();
        }


        /*
         this removes a selected forecast from a list and returns it.
         */
        public List<WeatherForecast> removeForecastFromList(int id) {

            List<WeatherForecast> list = getForecastsAsList();
            list.Remove(list.Find(x => x.Id == id));

            return list;
        }


        public List<WeatherLocation> removeCityFromList(int id)
        {

            List<WeatherLocation> list = getLocationsAsList();
            list.Remove(list.Find(x => x.Id == id));

            return list;
        }



        public List<WeatherLocation> getLocationsAsList() {

            StreamReader r = new StreamReader(CitiesJson);
            String CitiesString = r.ReadToEnd();
            List<WeatherLocation> ListOfLocations = JsonConvert.DeserializeObject<List<WeatherLocation>>(CitiesString);
            r.Close();
            return ListOfLocations;

        }

        public List<WeatherForecast> getForecastsAsList() {

            StreamReader r = new StreamReader(ForecastsJson);
            String ForecastsString = r.ReadToEnd();
            List<WeatherForecast> ListOfForecasts = JsonConvert.DeserializeObject<List<WeatherForecast>>(ForecastsString);
            r.Close();
            return ListOfForecasts;
        }
    }
}
