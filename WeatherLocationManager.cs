﻿/**
 * author Ari Talala 2020
 * 
 * This class is used to manage locations and is used in WeatherLocationController.
 * This class could be used to manage database connections should data be saved to a DB instead to files.
 * */


using System;

using System.Collections.Generic;
namespace FuusorNayte
{
    public class WeatherLocationManager
    {

        public List<WeatherLocation> locations = new List<WeatherLocation>();

        

        public WeatherLocationManager()
        {
           
            
        }


        public void addLocation(WeatherLocation location) {

            locations.Add(location);
            JsonStorageManager jsonStorageManager = new JsonStorageManager();
            jsonStorageManager.saveNewLocation(location);

             }

        public void removeLocationAt(int index) {

            locations.RemoveAt(index);
        }

        public List<WeatherLocation> getWeatherLocations() {
            return locations;
        }
    }
}
