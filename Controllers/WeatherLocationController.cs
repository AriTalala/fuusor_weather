﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;



namespace FuusorNayte.Controllers
{

    [ApiController]
    [Route("[controller]")]

    public class WeatherLocationController : ControllerBase
    {

        private readonly ILogger<WeatherLocationController> _logger;

        public WeatherLocationController(ILogger<WeatherLocationController> logger)
        {
            _logger = logger;
        }


        [HttpGet]
        public IEnumerable<WeatherLocation> Get()
        {
            var rng = new Random();

            WeatherLocationManager wlm = new WeatherLocationManager();

            JsonStorageManager jsonStorageManager = new JsonStorageManager();

            List<WeatherLocation> list = jsonStorageManager.getLocationsAsList();

            //List<WeatherLocation> list = wlm.getWeatherLocations();

            return Enumerable.Range(0, list.Count).Select(index => new WeatherLocation()
            {
                Id = list.ElementAt(index).Id,
                Name = list.ElementAt(index).Name
                //Name = Cities[rng.Next(index)]

            }).ToArray();
            
        }


        [HttpPost]
        public IEnumerable<WeatherLocation> PostNewLocation([FromBody] WeatherLocation location) { 
        //public IEnumerable<WeatherLocation> PostNewLocation(string name) {

            WeatherLocationManager manager = new WeatherLocationManager();


            /*These few lines here are used to get the previously set id-values of cities.
             This is needed so that we may randomize a new id value so that it does not collide with previous id-values.
            Thus we always have a unique id. The range of 10k could be increased by having it rather 10k*count of cities
            so each time the size of the cities storage grows, the id range increases further.
             */
            List<int> city_ids = new List<int>();
            JsonStorageManager man = new JsonStorageManager();

            List<WeatherForecast> Cities = man.getForecastsAsList();

            for (int i = 0; i < Cities.Count; i++)
            {
                city_ids.Add(Cities.ElementAt(i).Id);
            }

            var range = Enumerable.Range(1, 10000).Where(i => !city_ids.Contains(i));
            var random = new System.Random();
            int indexOfNewID = random.Next(0, 10000 - city_ids.Count);

            int newIdValue = range.ElementAt(indexOfNewID);
            location.Id = newIdValue;

            manager.addLocation(location);
            List<WeatherLocation> list =  man.getLocationsAsList();
            
            

            return Enumerable.Range(0, list.Count).Select(index => new WeatherLocation()
            {
                Id = list.ElementAt(index).Id,
                Name = list.ElementAt(index).Name,
                //Name = Cities[rng.Next(index)]

            }).ToArray();

        }

        [HttpDelete("{id}")]
        public IEnumerable<WeatherLocation> RemoveLocation([FromRoute] int id)
        {

            JsonStorageManager manager = new JsonStorageManager();
            manager.removeCity(id);
            List<WeatherLocation> list = manager.getLocationsAsList();


            return Enumerable.Range(0, list.Count).Select(index => new WeatherLocation()
            {
                Id = list.ElementAt(index).Id,
                Name = list.ElementAt(index).Name,

            }).ToArray();


        }



    }
}
