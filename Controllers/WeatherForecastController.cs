﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FuusorNayte.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        
        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }


        //[HttpDelete]
        [HttpDelete("{id}")]

        public IEnumerable<WeatherForecast> RemoveForecast([FromRoute] int id) {

            JsonStorageManager man = new JsonStorageManager();
            man.removeForecast(id);
            List<WeatherForecast> list = new JsonStorageManager().
                getForecastsAsList();
            
            
            return Enumerable.Range(0, list.Count).Select(index => new WeatherForecast
            {
                Id = list.ElementAt(index).Id,
                Date = list.ElementAt(index).Date,
                Wind = list.ElementAt(index).Wind,
                Rain = list.ElementAt(index).Rain,
                TemperatureC = list.ElementAt(index).TemperatureC,
                City = list.ElementAt(index).City,
            })
            .ToArray();

        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            var rngRain = new Random();
            var rngWind = new Random();


            List < WeatherForecast > list = new JsonStorageManager().
                getForecastsAsList();
            string datestring = list.ElementAt(0).Date;

            
            return Enumerable.Range(0, list.Count).Select(index => new WeatherForecast
            {
                Id = list.ElementAt(index).Id,
                Date = list.ElementAt(index).Date,
                Wind = list.ElementAt(index).Wind,
                Rain = list.ElementAt(index).Rain,
                TemperatureC = list.ElementAt(index).TemperatureC,
                City = list.ElementAt(index).City,
            })
            .ToArray();
        }

        /*
         Here forecast is saved to local JSON file forecasts and the list of updated forecasts is returned.
         */
        [HttpPost]
        public IEnumerable<WeatherForecast> PostNewForecast([FromBody] WeatherForecast forecast)
        {

            
            WeatherForecastManager manager = new WeatherForecastManager();

            JsonStorageManager man = new JsonStorageManager();

            /*These few lines here are used to get the previously set id-values of forecasts.
             This is needed so that we may randomize a new id value so that it does not collide with previous id-values.
            Thus we always have a unique id. The range of 10k could be increased by having it rather 10k*count of forecasts
            so each time the size of the forecasts storage grows, the id range increases further.
             */
            List<int> forecast_ids = new List<int>();
            List<WeatherForecast> PreviousForecasts = man.getForecastsAsList();

            for (int i = 0; i < PreviousForecasts.Count; i++)
            {
                forecast_ids.Add(PreviousForecasts.ElementAt(i).Id);
            }

            var range = Enumerable.Range(1, 10000).Where(i => !forecast_ids.Contains(i));
            var random = new System.Random();
            int indexOfNewID = random.Next(0, 10000 - forecast_ids.Count);

            int newIdValue = range.ElementAt(indexOfNewID);
            forecast.Id = newIdValue;


            manager.addForecast(forecast);



            List<WeatherForecast> list = man.getForecastsAsList();



            return Enumerable.Range(0, list.Count).Select(index => new WeatherForecast()
            {
                Date = list.ElementAt(index).Date,
               // Date = DateTime.Parse(list.ElementAt(index).ConvertedDate),
                Wind = list.ElementAt(index).Wind,
                Rain = list.ElementAt(index).Rain,
                TemperatureC = list.ElementAt(index).TemperatureC,
                City = list.ElementAt(index).City,
            }).ToArray();
            

        }
    }
}
